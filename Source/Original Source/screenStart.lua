------------------------------------------------------------------------------------------------------------------------------------
-- RED BIT ESCAPE Corona SDK Template
------------------------------------------------------------------------------------------------------------------------------------
-- Developed by Deep Blue Apps.com [www.deepbueapps.com]
------------------------------------------------------------------------------------------------------------------------------------
-- Abstract: Avoid the blue enemies. Stay alive as long as possible
------------------------------------------------------------------------------------------------------------------------------------
-- Release Version 1.0
-- Code developed for CORONA SDK STABLE RELEASE 2014.2189
-- 12th March 2014
------------------------------------------------------------------------------------------------------------------------------------
-- screenStart.lua
------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------
-- Require all of the external modules for this level
---------------------------------------------------------------
local ui 				= require("ui")
local widget 			= require("widget")
local myGlobalData 		= require("globalData")
local loadsave 			= require("loadsave")
local storyboard		= require( "storyboard" )
local changeTheScene	= false
local scene				= storyboard.newScene()

---------------------------------------------------------------
-- Define our SCENE variables and sprite object variables
---------------------------------------------------------------
local buttonGroup 		= display.newGroup()
local gameAreaGroup 	= display.newGroup()

local wallThickness = 7
local distanceFromEdge = 10
local maxSquareSize = myGlobalData._w - distanceFromEdge
local innerWallDistance = 40
local maxInnerSquareSize = maxSquareSize - innerWallDistance

local enemySize = 30
local enemyMoveSpeed = 250
local enemySpawnTable = {}

local playerSize = 30
local player
local scoreDisplay
local score = 0
local scoreTimertimer

local gameOver = false

local gameOverText
local restartText

-- create table
local stars = {}
 
-- initial vars
local stars_total = 60
local stars_field1= 200
local stars_field2= 200
local stars_field3= 600
local star_radius_min  = 1
local star_radius_max  = 2



local checkVelocityTimer
math.randomseed( os.time() )

------------------------------------------------------------------------------------------------------------------------------------
-- Setup the Physics World
------------------------------------------------------------------------------------------------------------------------------------
physics.start()
physics.setScale( 12 )
physics.setGravity( 0, 0 )
physics.setPositionIterations(128)

------------------------------------------------------------------------------------------------------------------------------------
-- un-comment to see the Physics world over the top of the Sprites
------------------------------------------------------------------------------------------------------------------------------------
--physics.setDrawMode( "hybrid" )


---------------------------------------------------------------
-- Called when the scene's view does not exist:
---------------------------------------------------------------
function scene:createScene( event )

	local screenGroup = self.view

	display.setDefault( "anchorX", 0.5 )
	display.setDefault( "anchorY", 0.5 )
	
	print("Main Game Scene..Loaded")
	
	local WallFilterData = { categoryBits = 1, maskBits = 6 }
	local PlayerFilterData = { categoryBits = 2, maskBits = 5 }
	local EnemyFilterData = { categoryBits = 4, maskBits = 3 }

	-----------------------------------------------------------------
	-- Add Score
	-----------------------------------------------------------------
	scoreDisplay = display.newText("Score: "..score,0,0, "HelveticaNeue-CondensedBlack", 22)
	scoreDisplay:setFillColor(1,1,1)
	scoreDisplay.x = myGlobalData._w/2
	scoreDisplay.y = 40
	scoreDisplay.alpha = 1
	screenGroup:insert( scoreDisplay )
		
	-----------------------------------------------------------------
	-- Add DBA Logo
	-----------------------------------------------------------------
    local Logo = display.newImageRect(screenGroup, myGlobalData.imagePath.."dbaLogo.png", 138,71);
	Logo.x = myGlobalData._w/2
	Logo.y = myGlobalData._h - 40
	screenGroup:insert( Logo )
	
	-----------------------------------------------------------------
	-- Add the Walls
	-----------------------------------------------------------------
	local function addwall(x ,y, sizeX, sizeY, Colour)
		local wallMaterial = { density=100.0, friction=0.0, bounce=1, filter=WallFilterData }
		local wall = display.newRect( 0,0, sizeX, sizeY )
		wall.x = x; wall.y = y
		if(Colour=="Blue")then
			wall:setFillColor(0.15,0.26,0.62)--blue
			wall.myName = "innerWall"
		else
			wall:setFillColor(0.7,0.2,0.1)--red
			wall.myName = "outerWall"
		end		
		physics.addBody( wall, "static", wallMaterial )
		gameAreaGroup:insert( wall )
		
	end
	--add OUTER Walls
	addwall(distanceFromEdge, myGlobalData._h/2, wallThickness, maxSquareSize, "Red"  ) --Left
	addwall(maxSquareSize, myGlobalData._h/2, wallThickness, maxSquareSize, "Red"  ) --Right
	addwall(myGlobalData._w/2, myGlobalData._h/2+(maxSquareSize/2), maxSquareSize-3, wallThickness, "Red"  ) --Bottom
	addwall(myGlobalData._w/2, myGlobalData._h/2-(maxSquareSize/2), maxSquareSize-3, wallThickness, "Red"  ) --Top

	--add OUTER Walls
	addwall(innerWallDistance-distanceFromEdge, myGlobalData._h/2, wallThickness, maxInnerSquareSize, "Blue"  ) --Left
	addwall(maxSquareSize-(innerWallDistance/2), myGlobalData._h/2, wallThickness, maxInnerSquareSize, "Blue"  ) --Right
	addwall(myGlobalData._w/2, myGlobalData._h/2+(maxInnerSquareSize/2), maxInnerSquareSize-3, wallThickness, "Blue"  ) --Bottom
	addwall(myGlobalData._w/2, myGlobalData._h/2-(maxInnerSquareSize/2), maxInnerSquareSize-3, wallThickness, "Blue"  ) --Top


	-----------------------------------------------------------------
	--Add the player to the screen
	-----------------------------------------------------------------
	player = display.newRect( 0,0, playerSize,playerSize )
	player:setFillColor(0.7,0.2,0.1, 0.4)--red
	player:setStrokeColor(1,1,1, 0.7)--white
	player.strokeWidth = 1
	player.myName = "Player"
	local playerMaterial = { density=100.0, friction=0.1, bounce=0.0, filter=PlayerFilterData }
	player.x = myGlobalData._w/2
	player.y = myGlobalData._h/2
	physics.addBody( player, "dynamic", playerMaterial )
	player.gravityScale = 0.0
	player.isSleepingAllowed = false
	player.isFixedRotation = true
	player.isBullet = true
	player.isSensor = true
	gameAreaGroup:insert(player)
	player:addEventListener( "touch", onTouch )


	-----------------------------------------------------------------
	--Add 4 x enemies to the screen
	-----------------------------------------------------------------
	local function spawnEnemy(params)
		local object = display.newRect( 0,0, enemySize,enemySize )
		object:setFillColor(0.2,0.6,0.8, 0.4)--blue
		object:setStrokeColor(1,1,1, 0.5)--white
		object.strokeWidth = 1
		
		object.objTable = params.objTable
		object.index = #object.objTable + 1
		object.myName = "Enemy"
		
		local enemyMaterial = { density=100.0, friction=0.1, bounce=0.0, filter=EnemyFilterData }
		object.x = params.startX
		object.y = params.startY
		physics.addBody( object, "dynamic", enemyMaterial )
		object.gravityScale = 0.0
		object.isSleepingAllowed = false
		object.linearDamping = 0
		object.isFixedRotation = true
		object.isBullet = true
		gameAreaGroup:insert(object)

		object.objTable[object.index] = object

		return object
	end

	-----------------------------------------------------------------
	--Create 4 Enemies
	-----------------------------------------------------------------
	local spawns = spawnEnemy( { startX = innerWallDistance+(enemySize-distanceFromEdge), startY = myGlobalData._h/2-(maxInnerSquareSize/2)+enemySize, objTable = enemySpawnTable, } ) --Top Left Enemy
	local spawns = spawnEnemy( { startX = maxSquareSize-(innerWallDistance/2)-enemySize, startY = myGlobalData._h/2-(maxInnerSquareSize/2)+enemySize, objTable = enemySpawnTable, } ) --Top Right Enemy
	local spawns = spawnEnemy( { startX = innerWallDistance+(enemySize-distanceFromEdge), startY = myGlobalData._h/2+(maxInnerSquareSize/2)-enemySize, objTable = enemySpawnTable, } ) --Bottom Left Enemy
	local spawns = spawnEnemy( { startX = maxSquareSize-(innerWallDistance/2)-enemySize, startY = myGlobalData._h/2+(maxInnerSquareSize/2)-enemySize, objTable = enemySpawnTable, } ) --Bottom Right Enemy


	-----------------------------------------------------------------
	-- GameOver Text (Hidden until needed)
	-----------------------------------------------------------------
	gameOverText = display.newText("GAME OVER",0,0, "HelveticaNeue-CondensedBlack", 44)
	gameOverText:setFillColor(0.5,1,0.7)
	gameOverText.x = myGlobalData._w/2
	gameOverText.y = -2000 --myGlobalData._h/2
	gameOverText.alpha = 1
	gameAreaGroup:insert( gameOverText )

	
	restartText = display.newText("Restart Game",0,0, "HelveticaNeue-CondensedBlack", 36)
	restartText:setFillColor(0.5,1,0.7)
	restartText.x = myGlobalData._w/2
	restartText.y = -2000 --myGlobalData._h/2
	restartText.alpha = 1
	restartText:addEventListener( "touch", restartTouched )
	gameAreaGroup:insert( restartText )

	-----------------------------------------------------------------
	--insert the game area into the main scene group
	-----------------------------------------------------------------
	screenGroup:insert( gameAreaGroup )


	-----------------------------------------------------------------
	--Start the Enemies moving
	-----------------------------------------------------------------		
	local function startMove()
		enemySpawnTable[1]:applyForce(500000,500000,enemySpawnTable[1].x,enemySpawnTable[1].y) -- Top Left Enemy
		enemySpawnTable[2]:applyForce(-500000,500000,enemySpawnTable[2].x,enemySpawnTable[2].y) -- Top Right Enemy
		enemySpawnTable[3]:applyForce(500000,-500000,enemySpawnTable[3].x,enemySpawnTable[3].y) -- Bottom Left Enemy
		enemySpawnTable[4]:applyForce(-500000,-500000,enemySpawnTable[4].x,enemySpawnTable[4].y) -- Bottom Right Enemy
	end
	local startEnemiesMovingTimer = timer.performWithDelay( 300, startMove)				

	
	-----------------------------------------------------------------
	--Update the score every 1 second
	-----------------------------------------------------------------		
	local function trackTimeUpdate()   -- RunTime enterFrame event handler
		score = score + 1
	end
	scoreTimertimer = timer.performWithDelay( 1000, trackTimeUpdate,10000)				
	
	
	
	
-- create/draw objects
for i = 1, stars_total do
        local star = {} 
        star.object = display.newCircle(math.random(display.contentWidth),math.random(display.contentHeight),math.random(star_radius_min,star_radius_max))
        stars[ i ] = star       
		screenGroup:insert( star.object )
end
 
 
	
	
	
	
	
end -- Setting up the CREATE SCENE function


------------------------------------------------------------------------------------------------------------------------------------
-- Update bg stars
------------------------------------------------------------------------------------------------------------------------------------
function udpdatestars(event)
	for i = stars_total,1, -1 do
		if (i < stars_field1) then
				stars[i].object:setFillColor(0.5,0.5,0.5)
				starspeed = 1
		end
		if (i < stars_field2 and i > stars_field1) then
				stars[i].object:setFillColor(0.7,0.7,0.7)
				starspeed = 2
		end
		if (i < stars_field3 and i > stars_field2) then
				stars[i].object:setFillColor(1,1,1)
				starspeed = 3
		end
		stars[i].object.y  = stars[i].object.y + starspeed      
		if (stars[i].object.y > display.contentHeight) then
				stars[i].object.y = stars[i].object.y-display.contentHeight
		end
	end
end





-----------------------------------------------------------------
-- Restart Text (Hidden until needed) - Button event
-----------------------------------------------------------------
function restartTouched( event )
	if event.phase == "began" then

	elseif event.phase == "ended" then
		--gameOver = false
		--print("Go Reset")
		storyboard.gotoScene( "screenReset",{time=20})	--This is our start screen
	end

	return true
end


---------------------------------------------------------------------------------------------
--Handle the player touch/move/dragging
---------------------------------------------------------------------------------------------
function onTouch( event )
	local t = event.target
	

	if (gameOver==false) then
		local phase = event.phase
		if "began" == phase then
			-- Make target the top-most object
			local parent = t.parent
			parent:insert( t )
			display.getCurrentStage():setFocus( t )

			t.isFocus = true

			-- Store initial position
			t.x0 = event.x - t.x
			t.y0 = event.y - t.y
		elseif t.isFocus then
			if "moved" == phase then
				t.x = event.x - t.x0
				t.y = event.y - t.y0
			elseif "ended" == phase or "cancelled" == phase then
				display.getCurrentStage():setFocus( nil )
				t.isFocus = false
			end
		end

		return true
	end
end



---------------------------------------------------------------------------------------------
-- Stabalise the Enemies movements & prevent sticking to walls
---------------------------------------------------------------------------------------------
local function resetLinearVelocity(event)
  local thisX, thisY = event:getLinearVelocity()

	local vel = enemyMoveSpeed -- this is the arbitrary speed of player motion.
	local p1Rads = math.random(180)

	local velX = math.cos (p1Rads) * vel 
	local velY = math.sin (p1Rads) * vel

	if thisY == 0 then
	  thisY = -event.lastY
	end
	if thisX == 0 then
	  thisX = -event.lastX
	end
  
	event:setLinearVelocity(velX,velY)
	event.lastX, event.lastY = thisX, thisY

end


---------------------------------------------------------------------------------------------
--Game Collision logic system
---------------------------------------------------------------------------------------------
local function onGlobalCollision( event )

	if ( event.phase == "began" and gameOver==false) then
	
		if (event.object1.myName == "innerWall" and event.object2.myName == "Enemy" ) then
			checkVelocityTimer = timer.performWithDelay(0, resetLinearVelocity(event.object2))
		end

		---------------------------------------------------------------------------------------------
		--Player hit logic
		---------------------------------------------------------------------------------------------
		if (event.object1.myName == "Enemy" and event.object2.myName == "Player" ) then
				gameOver = true
		end
		if (event.object1.myName == "Player" and event.object2.myName == "Enemy" ) then
				gameOver = true
		end
		if (event.object1.myName == "Player" and event.object2.myName == "innerWall" ) then
				gameOver = true
		end
		if (event.object1.myName == "innerWall" and event.object2.myName == "Player" ) then
				gameOver = true
		end


	end

end


---------------------------------------------------------------
-- Game Update core
---------------------------------------------------------------
function updateTick(event)

	if(gameOver==true) then
	
		player:removeEventListener( "touch", onTouch )		
		display.remove(player)
		player = nil
		

		--Move the player
		for i = 1, #enemySpawnTable do
			--print(enemySpawnTable[i].index)
			enemySpawnTable[i]:setLinearVelocity(0,0)

		end
		
		 --> Cancel the timer
		if(scoreTimertimer) then
			timer.cancel(scoreTimertimer)
			scoreTimertimer = nil
		end
		
		--Show the Game Over & restart message
		gameOverText.y = myGlobalData._h/2
		restartText.y = myGlobalData._h/2+40

		
		
	--Cancel events	
	Runtime:removeEventListener( "enterFrame", updateTick )
	Runtime:removeEventListener ( "collision", onGlobalCollision )
	Runtime:removeEventListener("enterFrame",udpdatestars)	

		
	end
	
	if(gameOver==false) then
		display.remove(scoreDisplay)
		scoreDisplay = nil
		scoreDisplay = display.newText("Score: "..score,0,0, "HelveticaNeue-CondensedBlack", 36)
		scoreDisplay:setFillColor(1,1,1)
		scoreDisplay.x = myGlobalData._w/2
		scoreDisplay.y = 40
		scoreDisplay.alpha = 1
	end

end



-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	
	local previousScene = storyboard.getPrevious()
	print(previousScene)
		
	if (previousScene ~= nil ) then
		local previousInfo = "Previous Scene: "..previousScene
		print(previousInfo)

		storyboard.purgeScene( previousScene )
		storyboard.removeScene( previousScene )
	end
	
	storyboard.purgeScene( "screenReset" )
	storyboard.removeScene( "screenReset" )
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )

	--Runtime:removeEventListener( "enterFrame", updateTick )
	--Runtime:removeEventListener ( "collision", onGlobalCollision )

	display.remove(scoreDisplay)
	scoreDisplay = nil

	 --> Cancel the timer
	--if(scoreTimertimer) then
	--	timer.cancel(scoreTimertimer)
	--	scoreTimertimer = nil
	--end
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
    --local group = self.view
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------





---------------------------------------------------------------
-- Add a enterFrame event
---------------------------------------------------------------
Runtime:addEventListener( "enterFrame", updateTick )
Runtime:addEventListener ( "collision", onGlobalCollision )
Runtime:addEventListener("enterFrame",udpdatestars)	

return scene