------------------------------------------------------------------------------------------------------------------------------------
-- RED BIT ESCAPE Corona SDK Template
------------------------------------------------------------------------------------------------------------------------------------
-- Developed by Deep Blue Apps.com [www.deepbueapps.com]
------------------------------------------------------------------------------------------------------------------------------------
-- Abstract: Avoid the blue enemies. Stay alive as long as possible
------------------------------------------------------------------------------------------------------------------------------------
-- Release Version 1.0
-- Code developed for CORONA SDK STABLE RELEASE 2014.2189
-- 12th March 2014
------------------------------------------------------------------------------------------------------------------------------------
-- screenReset.lua
------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------
-- Require all of the external modules for this level
---------------------------------------------------------------
local storyboard		= require( "storyboard" )
local scene				= storyboard.newScene()

local sceneChangeTimer

---------------------------------------------------------------
-- Called when the scene's view does not exist:
---------------------------------------------------------------
function scene:createScene( event )

	local screenGroup = self.view

	print("Reset Game Scene..Loaded")

	-----------------------------------------------------------------
	--Update the score every 1 second
	-----------------------------------------------------------------		
	local function gotoGame()
		if(sceneChangeTimer) then
			timer.cancel(sceneChangeTimer)
			sceneChangeTimer = nil
		end
		print("---------------------------------")
		print("Going to game Scene....")
		storyboard.gotoScene( "screenStart",{time=20})	--This is our start screen
	end
	sceneChangeTimer = timer.performWithDelay( 100, gotoGame)				
	
	
	
	
end -- Setting up the CREATE SCENE function





-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	storyboard.purgeScene( "screenStart" )
	storyboard.removeScene( "screenStart" )
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )


end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
    --local group = self.view
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------



return scene